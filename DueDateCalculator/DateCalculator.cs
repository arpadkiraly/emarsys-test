﻿using System;

namespace DueDateCalculator
{
    internal static class DateCalculator
    {
        public static DateTimeOffset AddWorkTimeSpan(this DateTimeOffset baseDateTime, WorkTimeSpan workTimeSpan)
        {
            // add hours
            DateTimeOffset calculatedDate = baseDateTime.AddHours(workTimeSpan.NumberOfWorkHours);
            if (calculatedDate > new DateTimeOffset(baseDateTime.Year, baseDateTime.Month, baseDateTime.Day, 17, 0, 0, baseDateTime.Offset))
                calculatedDate = calculatedDate.AddHours(16);
            if (calculatedDate.DayOfWeek == DayOfWeek.Saturday)
                calculatedDate = calculatedDate.AddDays(2);

            // add days
            calculatedDate = calculatedDate.AddDays(workTimeSpan.NumberOfWorkDays);
            if (DateTimeValidator.IsWeekendDay(calculatedDate))
                calculatedDate = calculatedDate.AddDays(2);

            // add weeks
            calculatedDate = calculatedDate.AddDays(7 * workTimeSpan.NumberOfWorkWeeks);

            return calculatedDate;
        }
    }
}