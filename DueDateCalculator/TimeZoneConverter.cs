﻿using System;

namespace DueDateCalculator
{
    internal static class TimeZoneConverter
    {
        internal static DateTimeOffset GetSubmissionDateTimeInReferenceTimeZone(DateTimeOffset submissionDateTime, TimeSpan referenceTimeOffset)
        {
            return submissionDateTime.ToOffset(referenceTimeOffset);
        }

        internal static DateTimeOffset GetCalculatedDueDateInSenderTimeZone(DateTimeOffset calculatedDateDate, TimeSpan senderTimeOffset)
        {
            return calculatedDateDate.ToOffset(senderTimeOffset);
        }
    }
}