﻿using System;

namespace DueDateCalculator
{
    public interface IDueDateCalculator
    {
        DateTimeOffset CalculateDueDate(DateTimeOffset submissionDateTime, int turnaroundTimeInHours);
    }
}