﻿using System;

namespace DueDateCalculator
{
    public static class DateTimeValidator
    {
        public static bool IsWeekendDay(DateTimeOffset dateTime)
        {
            return dateTime.DayOfWeek == DayOfWeek.Saturday || dateTime.DayOfWeek == DayOfWeek.Sunday;
        }

        public static bool IsBetween9amAnd5pm(DateTimeOffset dateTime)
        {
            if (dateTime > new DateTimeOffset(dateTime.Year, dateTime.Month, dateTime.Day, 17, 0, 0, dateTime.Offset))
                return false;

            if (dateTime < new DateTimeOffset(dateTime.Year, dateTime.Month, dateTime.Day, 9, 0, 0, dateTime.Offset))
                return false;

            return true;
        }
    }
}