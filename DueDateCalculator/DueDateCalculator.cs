﻿using System;

namespace DueDateCalculator
{
    // This is the main class whose CalculateDueDate method is called by consumers.
    //
    // Assumptions made:
    // 1. It was not stated whether future datetimes should be accepted or not, so we are not setting up any limitations - all datetimes are accepted, including future datetimes.
    // 2. It was not stated whether boundary submission times are accepted or not, we accept them for now (e.g.: 17:00:00 is a valid submission time whereas 17:00:01 is not).
    // 3. It was not stated whether turnaround time can be provided as a fraction or it is an integer. For the sake of simplicity, only integral turnaround time hours are accepted.
    // 4. It is assumed that the consumer (who reports a problem) is not necessarily in the same time zone as those who fix the problem.
    //    Working hours are considered in the time zone of those who fix the problem.
    // 5. Also for the sake of simplicity, dependency injection libraries are not used for such a primitive solution.
    // 6. Under normal circumstances, a number of parameters could be made configurable, but I did not take the time to do that for the sake of simplicity. Some examples:
    //     - weekenddays (in some countries Sunday is a weekday)
    //     - working hours (what if in some countries the working hours are 8am to 4pm)
    //     - number of working hours a day (it could be 6 or 7 instead of 8)
    //     - turnaround time lower and upper limits
    //     - etc.
    public class DueDateCalculator : IDueDateCalculator
    {
        private readonly TimeSpan _referenceTimeOffset;

        public DueDateCalculator(TimeSpan referenceTimeOffset)
        {
            _referenceTimeOffset = referenceTimeOffset;
        }

        public DateTimeOffset CalculateDueDate(DateTimeOffset submissionDateTime, int turnaroundTimeInHours)
        {
            var submissionDateTimeInReferenceTimeZone = TimeZoneConverter.GetSubmissionDateTimeInReferenceTimeZone(submissionDateTime, _referenceTimeOffset);

            DueDateCalculatorParameterValidator.Validate(submissionDateTimeInReferenceTimeZone, turnaroundTimeInHours);

            var calculatedDueDate = CalculateDueDateForValidParameters(submissionDateTimeInReferenceTimeZone, turnaroundTimeInHours);

            return TimeZoneConverter.GetCalculatedDueDateInSenderTimeZone(calculatedDueDate, submissionDateTime.Offset);
        }
        
        private static DateTimeOffset CalculateDueDateForValidParameters(DateTimeOffset submissionDateTime, int turnaroundTimeInHours)
        {
            return submissionDateTime.AddWorkTimeSpan(new WorkTimeSpan(turnaroundTimeInHours));
        }
    }
}
