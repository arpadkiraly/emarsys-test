﻿using System;
using Xunit;

namespace DueDateCalculator.TDDTest
{
    public class DateTimeValidatorTest
    {
        [Theory]
        [InlineData(2019, 6, 17)]   // a Monday
        [InlineData(2019, 7, 30)]   // a Tuesday
        [InlineData(2018, 10, 3)]   // a Wednesday
        [InlineData(2037, 1, 15)]   // a Thursday
        [InlineData(1976, 1, 16)]   // a Friday
        public void IsWeekendDay_VariousWeekdays_ShouldReturnFalse(int year, int month, int day)
        {
            // Arrange
            const bool expected = false;

            // Act
            var actual = DateTimeValidator.IsWeekendDay(new DateTimeOffset(year, month, day, 15, 0, 0, new TimeSpan(0, 0, 0)));

            // Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(2019, 6, 22)]   // a Saturday
        [InlineData(2019, 8, 3)]    // another Saturday
        [InlineData(2017, 11, 5)]   // a Sunday
        [InlineData(2032, 2, 15)]   // another Sunday
        public void IsWeekendDay_VariousWeekenddays_ShouldReturnTrue(int year, int month, int day)
        {
            // Arrange
            const bool expected = true;

            // Act
            var actual = DateTimeValidator.IsWeekendDay(new DateTimeOffset(year, month, day, 15, 0, 0, new TimeSpan(0, 0, 0)));

            // Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(8, 59, 59)]
        [InlineData(0, 0, 0)]
        [InlineData(17, 0, 1)]
        [InlineData(6, 30, 1)]
        [InlineData(21, 5, 56)]
        public void IsBetween9amAnd5pm_OutsideHours_ShouldReturnFalse(int hour, int minute, int second)
        {
            // Arrange
            const bool expected = false;

            // Act
            var actual = DateTimeValidator.IsBetween9amAnd5pm(new DateTimeOffset(2019, 1, 1, hour, minute, second, new TimeSpan(0, 0, 0)));

            // Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(9, 0, 0)]
        [InlineData(17, 0, 0)]
        [InlineData(11, 1, 34)]
        [InlineData(15, 30, 59)]
        [InlineData(12, 0, 0)]
        public void IsBetween9amAnd5pm_WithinHours_ShouldReturnTrue(int hour, int minute, int second)
        {
            // Arrange
            const bool expected = true;

            // Act
            var actual = DateTimeValidator.IsBetween9amAnd5pm(new DateTimeOffset(2019, 1, 1, hour, minute, second, new TimeSpan(0, 0, 0)));

            // Assert
            Assert.Equal(expected, actual);
        }
    }

}