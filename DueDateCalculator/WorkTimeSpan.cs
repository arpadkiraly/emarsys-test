﻿namespace DueDateCalculator
{
    internal class WorkTimeSpan
    {
        private const int NUMBER_OF_WORK_HOURS_IN_A_DAY = 8;
        private const int NUMBER_OF_WORK_DAYS_IN_A_WEEK = 5;

        public int NumberOfWorkWeeks { get; }
        public int NumberOfWorkDays { get; }
        public int NumberOfWorkHours { get; }

        public WorkTimeSpan(int numberOfWorkHours)
        {
            var days = numberOfWorkHours / NUMBER_OF_WORK_HOURS_IN_A_DAY;
            var weeks = days / NUMBER_OF_WORK_DAYS_IN_A_WEEK;
            days = days - weeks * NUMBER_OF_WORK_DAYS_IN_A_WEEK;
            var hours = numberOfWorkHours % NUMBER_OF_WORK_HOURS_IN_A_DAY;

            NumberOfWorkWeeks = weeks;
            NumberOfWorkDays = days;
            NumberOfWorkHours = hours;
        }
    }
}