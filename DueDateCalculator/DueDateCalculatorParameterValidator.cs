﻿using System;

namespace DueDateCalculator
{
    internal static class DueDateCalculatorParameterValidator
    {
        private const int TURNAROUND_TIME_LOWER_LIMIT_IN_HOURS = 0;
        private const int TURNAROUND_TIME_UPPER_LIMIT_IN_HOURS = 8760; // = 1 year

        public static void Validate(DateTimeOffset submissionDateTime, int turnaroundTimeInHours)
        {
            if (DateTimeValidator.IsWeekendDay(submissionDateTime))
                throw new ArgumentException($"Submission date must be a work day in the reference timezone.", nameof(submissionDateTime));

            if (!DateTimeValidator.IsBetween9amAnd5pm(submissionDateTime))
                throw new ArgumentException($"Submission time must be between 9am and 5pm in the reference time zone.", nameof(submissionDateTime));

            if (turnaroundTimeInHours <= TURNAROUND_TIME_LOWER_LIMIT_IN_HOURS)
                throw new ArgumentException($"Turnaround time must be in hours and its value must be greater than {TURNAROUND_TIME_LOWER_LIMIT_IN_HOURS}.", nameof(turnaroundTimeInHours));

            // this was not a business requirement, only common sense to introduce a rational upper limit
            // normally this would not necessarily be an exception, maybe just a warning in the logs
            if (turnaroundTimeInHours >= TURNAROUND_TIME_UPPER_LIMIT_IN_HOURS)
                throw new ArgumentException($"Turnaround time must be in hours and its value must be lower than {TURNAROUND_TIME_UPPER_LIMIT_IN_HOURS}.", nameof(turnaroundTimeInHours));
        }
    }
}