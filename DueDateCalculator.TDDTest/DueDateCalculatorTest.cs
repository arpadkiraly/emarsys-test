using System;
using Xunit;

namespace DueDateCalculator.TDDTest
{
    public class DueDateCalculatorTest
    {
        [Theory]
        [InlineData(2019, 6, 25, 8, 59, 59)]
        [InlineData(2019, 6, 25, 17, 0, 1)]
        [InlineData(2019, 6, 25, 6, 15, 0)]
        [InlineData(2019, 6, 25, 21, 52, 45)]
        [InlineData(2019, 6, 25, 1, 30, 0)]
        public void CalculateDueDate_NoTimeOffset_SubmissionTimeIsOutsideOfWorkHours_ShouldThrowException(int year, int month, int day, int hour, int minute, int second)
        {
            // Arrange
            var calculator = new DueDateCalculator(new TimeSpan(0, 0, 0));

            // Act
            void Actual() =>
                calculator.CalculateDueDate(new DateTimeOffset(year, month, day, hour, minute, second, new TimeSpan(0, 0, 0)), 8);

            // Assert
            Assert.Throws<ArgumentException>((Action)Actual);
        }

        [Theory]
        [InlineData(2019, 6, 24, 23, 59, 59)]
        [InlineData(2019, 6, 25, 8, 0, 1)]
        [InlineData(2019, 6, 24, 21, 15, 0)]
        [InlineData(2019, 6, 25, 9, 0, 0)]
        [InlineData(2019, 6, 25, 12, 0, 0)]
        public void CalculateDueDate_WithTimeOffset_SubmissionTimeIsOutsideOfWorkHours_ShouldThrowException(int year, int month, int day, int hour, int minute, int second)
        {
            // Arrange
            var calculator = new DueDateCalculator(new TimeSpan(9, 0, 0));

            // Act
            void Actual() =>
                calculator.CalculateDueDate(new DateTimeOffset(year, month, day, hour, minute, second, new TimeSpan(0, 0, 0)), 8);

            // Assert
            Assert.Throws<ArgumentException>((Action)Actual);
        }

        [Theory]
        [InlineData(2019, 6, 22, 9, 30, 0)]
        [InlineData(2017, 11, 5, 9, 30, 0)]
        public void CalculateDueDate_NoTimeOffset_SubmissionDateIsNotAWorkDay_ShouldThrowException(int year, int month, int day, int hour, int minute, int second)
        {
            // Arrange
            var calculator = new DueDateCalculator(new TimeSpan(0, 0, 0));

            // Act
            void Actual() =>
                calculator.CalculateDueDate(new DateTimeOffset(year, month, day, hour, minute, second, new TimeSpan(0, 0, 0)), 8);

            // Assert
            Assert.Throws<ArgumentException>((Action)Actual);
        }

        [Theory]
        [InlineData(2019, 6, 25, 17, 59, 59)]
        [InlineData(2019, 6, 26, 2, 0, 1)]
        [InlineData(2019, 6, 25, 4, 0, 12)]
        [InlineData(2019, 6, 25, 12, 0, 0)]
        public void CalculateDueDate_WithTimeOffset_SubmissionDateIsNotAWorkDay_ShouldThrowException(int year, int month, int day, int hour, int minute, int second)
        {
            // Arrange
            var calculator = new DueDateCalculator(new TimeSpan(-9, 0, 0));

            // Act
            void Actual() =>
                calculator.CalculateDueDate(new DateTimeOffset(year, month, day, hour, minute, second, new TimeSpan(0, 0, 0)), 8);

            // Assert
            Assert.Throws<ArgumentException>((Action)Actual);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-1000.0)]
        [InlineData(-8716326)]
        public void CalculateDueDate_NoTimeOffset_TurnAroundTimeIsNegativeOrZero_ShouldThrowException(int turnaroundTimeInHours)
        {
            // Arrange
            var calculator = new DueDateCalculator(new TimeSpan(0, 0, 0));

            // Act
            void Actual() =>
                calculator.CalculateDueDate(new DateTimeOffset(2019, 6, 25, 9, 30, 0, new TimeSpan(0, 0, 0)), turnaroundTimeInHours);

            // Assert
            Assert.Throws<ArgumentException>((Action)Actual);
        }

        [Theory]
        [InlineData(8761)]
        [InlineData(100000)]
        [InlineData(123456789)]
        public void CalculateDueDate_NoTimeOffset_TurnAroundTimeIsGreatThan8760_ShouldThrowException(int turnaroundTimeInHours)
        {
            // Arrange
            var calculator = new DueDateCalculator(new TimeSpan(0, 0, 0));

            // Act
            void Actual() =>
                calculator.CalculateDueDate(new DateTimeOffset(2019, 6, 25, 9, 30, 0, new TimeSpan(0, 0, 0)), turnaroundTimeInHours);

            // Assert
            Assert.Throws<ArgumentException>((Action)Actual);
        }

        [Fact]
        public void CalculateDueDate_NoTimeOffset_TurnAroundTimeWithinDay_ShouldReturnCorrectDueDate()
        {
            // Arrange
            var calculator = new DueDateCalculator(new TimeSpan(0, 0, 0));
            var expected = new DateTimeOffset(2019, 6, 25, 12, 45, 30, new TimeSpan(0, 0, 0));

            // Act
            var actual = calculator.CalculateDueDate(new DateTimeOffset(2019, 6, 25, 9, 45, 30, new TimeSpan(0, 0, 0)), 3);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateDueDate_WithTimeOffset_TurnAroundTimeWithinDay_ShouldReturnCorrectDueDate()
        {
            // Arrange
            var calculator = new DueDateCalculator(new TimeSpan(4, 0, 0));
            var expected = new DateTimeOffset(2019, 6, 25, 10, 45, 30, new TimeSpan(0, 0, 0));

            // Act
            var actual = calculator.CalculateDueDate(new DateTimeOffset(2019, 6, 25, 9, 45, 30, new TimeSpan(0, 0, 0)), 1);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateDueDate_NoTimeOffset_TurnAroundTimeNextDayWithinWeek_ShouldReturnCorrectDueDate()
        {
            // Arrange
            var calculator = new DueDateCalculator(new TimeSpan(0, 0, 0));
            var expected = new DateTimeOffset(2019, 6, 26, 9, 30, 0, new TimeSpan(0, 0, 0));

            // Act
            var actual = calculator.CalculateDueDate(new DateTimeOffset(2019, 6, 25, 10, 30, 0, new TimeSpan(0, 0, 0)), 7);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateDueDate_WithTimeOffset_TurnAroundTimeNextDayWithinWeek_ShouldReturnCorrectDueDate()
        {
            // Arrange
            var calculator = new DueDateCalculator(new TimeSpan(-3, 0, 0));
            var expected = new DateTimeOffset(2019, 6, 26, 12, 30, 0, new TimeSpan(0, 0, 0));

            // Act
            var actual = calculator.CalculateDueDate(new DateTimeOffset(2019, 6, 25, 13, 30, 0, new TimeSpan(0, 0, 0)), 7);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateDueDate_NoTimeOffset_TurnAroundTimeNextDayNextWeek_ShouldReturnCorrectDueDate()
        {
            // Arrange
            var calculator = new DueDateCalculator(new TimeSpan(0, 0, 0));
            var expected = new DateTimeOffset(2019, 7, 1, 11, 12, 56, new TimeSpan(0, 0, 0));

            // Act
            var actual = calculator.CalculateDueDate(new DateTimeOffset(2019, 6, 28, 16, 12, 56, new TimeSpan(0, 0, 0)), 3);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateDueDate_WithTimeOffset_TurnAroundTimeNextDayNextWeek_ShouldReturnCorrectDueDate()
        {
            // Arrange
            var calculator = new DueDateCalculator(new TimeSpan(-1, 0, 0));
            var expected = new DateTimeOffset(2019, 7, 1, 11, 12, 56, new TimeSpan(0, 0, 0));

            // Act
            var actual = calculator.CalculateDueDate(new DateTimeOffset(2019, 6, 28, 16, 12, 56, new TimeSpan(0, 0, 0)), 3);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateDueDate_NoTimeOffset_TurnAroundTimeSomeDayThisWeek_ShouldReturnCorrectDueDate()
        {
            // Arrange
            var calculator = new DueDateCalculator(new TimeSpan(0, 0, 0));
            var expected = new DateTimeOffset(2019, 6, 27, 13, 0, 0, new TimeSpan(0, 0, 0));

            // Act
            var actual = calculator.CalculateDueDate(new DateTimeOffset(2019, 6, 24, 17, 0, 0, new TimeSpan(0, 0, 0)), 20);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateDueDate_WithTimeOffset_TurnAroundTimeSomeDayThisWeek_ShouldReturnCorrectDueDate()
        {
            // Arrange
            var calculator = new DueDateCalculator(new TimeSpan(-2, 0, 0));
            var expected = new DateTimeOffset(2019, 6, 27, 12, 0, 0, new TimeSpan(-3, 0, 0));

            // Act
            var actual = calculator.CalculateDueDate(new DateTimeOffset(2019, 6, 24, 16, 0, 0, new TimeSpan(-3, 0, 0)), 20);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateDueDate_NoTimeOffset_TurnAroundTimeSomeDayNextWeek_ShouldReturnCorrectDueDate()
        {
            // Arrange
            var calculator = new DueDateCalculator(new TimeSpan(0, 0, 0));
            var expected = new DateTimeOffset(2019, 7, 2, 14, 0, 0, new TimeSpan(0, 0, 0));

            // Act
            var actual = calculator.CalculateDueDate(new DateTimeOffset(2019, 6, 25, 12, 0, 0, new TimeSpan(0, 0, 0)), 42);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateDueDate_WithTimeOffset_TurnAroundTimeSomeDayNextWeek_ShouldReturnCorrectDueDate()
        {
            // Arrange
            var calculator = new DueDateCalculator(new TimeSpan(-1, 0, 0));
            var expected = new DateTimeOffset(2019, 7, 3, 14, 30, 1, new TimeSpan(0, 0, 0));

            // Act
            var actual = calculator.CalculateDueDate(new DateTimeOffset(2019, 6, 25, 12, 30, 1, new TimeSpan(0, 0, 0)), 50);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateDueDate_NoTimeOffset_TurnAroundTimeInAFewWeeks_ShouldReturnCorrectDueDate()
        {
            // Arrange
            var calculator = new DueDateCalculator(new TimeSpan(0, 0, 0));
            var expected = new DateTimeOffset(2019, 7, 30, 14, 0, 0, new TimeSpan(0, 0, 0));

            // Act
            var actual = calculator.CalculateDueDate(new DateTimeOffset(2019, 7, 1, 12, 0, 0, new TimeSpan(0, 0, 0)), 170);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateDueDate_WithTimeOffset_TurnAroundTimeInAFewWeeks_ShouldReturnCorrectDueDate()
        {
            // Arrange
            var calculator = new DueDateCalculator(new TimeSpan(3, 0, 0));
            var expected = new DateTimeOffset(2019, 7, 31, 6, 0, 0, new TimeSpan(-2, 0, 0));

            // Act
            var actual = calculator.CalculateDueDate(new DateTimeOffset(2019, 7, 1, 12, 0, 0, new TimeSpan(-2, 0, 0)), 170);

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
